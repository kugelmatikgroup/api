package de.kks.api;

import java.util.ArrayList;
import java.util.List;

public class Coordinate {
    private int clusterX;

    private int clusterY;

    private int sphereX;

    private int sphereY;

    public Coordinate() {
        this.clusterX = -1;
        this.clusterY = -1;
        this.sphereX = -1;
        this.sphereY = -1;
    }

    public Coordinate(int clusterX, int clusterY) {
        this.clusterX = clusterX;
        this.clusterY = clusterY;
        this.sphereX = -1;
        this.sphereY = -1;
    }

    public Coordinate(int clusterX, int clusterY, int sphereX, int sphereY) {
        this.clusterX = clusterX;
        this.clusterY = clusterY;
        this.sphereX = sphereX;
        this.sphereY = sphereY;
    }

    @Override
    public String toString() {
        return "[" +
                clusterX + "/" +
                clusterY + "/" +
                sphereX + "/" +
                sphereY +
                "]";
    }

    public static String serializeList(List<Coordinate> coordinates) {
        //ignore ugly code ^^
        String coorFoo = "[";
        for (int i = 0; i < coordinates.size(); i++) {
            coorFoo += coordinates.get(i).toString() + (i==(coordinates.size()-1) ? "" : ",");
        }
        return coorFoo + "]";
    }

    public static List<Coordinate> deserializeCoordinate(String s) {
        List<Coordinate> deserializedCoordinates = new ArrayList<>();

        //example: [5/5/-1/-1]
        if (s.length() == 0) return null;
        String foo = s.substring(1,s.length()-1);
        String[] coordinates = foo.split("/");

        return deserializedCoordinates;
    }

    public int getClusterX() {
        return clusterX;
    }

    public void setClusterX(int clusterX) {
        this.clusterX = clusterX;
    }

    public int getClusterY() {
        return clusterY;
    }

    public void setClusterY(int clusterY) {
        this.clusterY = clusterY;
    }

    public int getSphereX() {
        return sphereX;
    }

    public void setSphereX(int sphereX) {
        this.sphereX = sphereX;
    }

    public int getSphereY() {
        return sphereY;
    }

    public void setSphereY(int sphereY) {
        this.sphereY = sphereY;
    }
}
