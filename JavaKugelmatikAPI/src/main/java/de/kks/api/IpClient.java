package de.kks.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import de.kks.api.command.Command;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class IpClient {
    ApiHandler apiHandler;
    private final String PREFIX = "IpClient ->";

    private String host = "127.0.0.1";
    private int port = 6543;
    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    public IpClient(ApiHandler apiHandler) {
        this.apiHandler = apiHandler;
    }

    public IpClient(ApiHandler apiHandler, String host, int port) {
        this.apiHandler = apiHandler;
        this.host = host;
        this.port = port;
    }

    public void connect() {
        try {
            socket = new Socket(host, port);
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            System.out.println(PREFIX + " Verbindung hergestellt");

            awaitInitiationProtocol();
        } catch (IOException e) {
            System.out.println("Socket konnte keine Verbindung zum Server aufbauen! \nUeberpruefe die angegebenen Serverdaten.");
            System.exit(-1);
            //e.printStackTrace();
        }
    }

    public void send(String message) {
        try {
            dataOutputStream.writeUTF(message);
            System.out.println(PREFIX + "Sent: " + message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String receiveMessage() {
        try {
            return dataInputStream.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Object handleCommandConversation(Package pac) {
        send(pac.serialize());;

        switch (apiHandler.getCommandHandler().getCommandByName(pac.getCmdName()).getCommandType()) {
            case ACTION -> {
                return receiveMessage();
            }
            case INFORMATION -> {
                String answer = receiveMessage();

                if (pac.getArgs().containsKey("coordinates")) {
                    Gson gson = new Gson();
                    Type mapType = new TypeToken<Map<String, Short>>(){}.getType();
                    Map<String, Short> foo = gson.fromJson(answer, mapType);

                    HashMap<Coordinate, Short> coordinateHeights = new HashMap<>();
                    foo.entrySet().forEach(all -> {
                        String[] coords = all.getKey().substring(1,all.getKey().length()-1).split("/");
                        coordinateHeights.put(new Coordinate(Integer.parseInt(coords[0]),Integer.parseInt(coords[1]),Integer.parseInt(coords[2]),Integer.parseInt(coords[3])), all.getValue());
                    });
                    return coordinateHeights;
                }
                return answer;
            }
            default -> {
                return null;
            }
        }
    }

    public void awaitInitiationProtocol() {
        String received = receiveMessage();
        System.out.println(PREFIX + "InitProtocolReceived: " + received);

        Gson gson = new Gson();
        InitProtocolData initProtocolData = gson.fromJson(received, InitProtocolData.class);

        try {
            ApiConfig.KUGELMATIK_WIDTH = initProtocolData.kugelmatikWidth;
            ApiConfig.KUGELMATIK_HEIGHT = initProtocolData.kugelmatikHeight;
            ApiConfig.CLUSTER_WIDTH = initProtocolData.clusterWidth;
            ApiConfig.CLUSTER_HEIGHT = initProtocolData.clusterHeight;
            ApiConfig.KUGELMATIK_MAX_HEIGHT = initProtocolData.maxHeight;
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }

        for (Map.Entry<String, Command> all : initProtocolData.commands.entrySet()) {
            apiHandler.getCommandHandler().registeredCommands.put(all.getKey(), all.getValue());
        }
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
