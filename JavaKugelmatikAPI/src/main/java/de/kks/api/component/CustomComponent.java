package de.kks.api.component;

import java.net.Inet4Address;

public abstract class CustomComponent {

    public String componentName;
    public String description;
    public String version;
    private Inet4Address ipAddress;

    public CustomComponent(String componentName, Inet4Address ipAddress, String version, String description) {
        this.componentName = componentName;
        this.version = version;
        this.ipAddress = ipAddress;
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public Inet4Address getIpAddress() {
        return ipAddress;
    }

    public String getVersion() {
        return version;
    }

    public String getComponentName() {
        return componentName;
    }

    public abstract String execute(String... args);
}
