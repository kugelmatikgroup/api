package de.kks.api;

public class ApiConfig {

    /**
     * The amount of Steppers that represent the width of a Cluster.
     */
    public static int CLUSTER_WIDTH;

    /**
     * The amount of Steppers that represent the height of a Cluster.
     */
    public static int CLUSTER_HEIGHT;

    /**
     * The amount of Clusters that represent the width of the Kugelmatik.
     */
    public static int KUGELMATIK_WIDTH;

    /**
     * The amount of Clusters that represent the height of the Kugelmatik.
     */
    public static int KUGELMATIK_HEIGHT;

    /**
     * The maximal height of a stepper. Range: 0-8000
     */
    public static int KUGELMATIK_MAX_HEIGHT;
}
