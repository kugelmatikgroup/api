package de.kks.api.exceptions;

public class UnregisteredCommandException extends Exception {
    public UnregisteredCommandException() {super("Trying to perform an unregistered command."); }
    public UnregisteredCommandException(String msg) {super("Trying to perform an unregistered command: " + msg); }
}
