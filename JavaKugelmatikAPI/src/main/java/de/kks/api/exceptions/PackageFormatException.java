package de.kks.api.exceptions;

public class PackageFormatException extends Exception {
    public PackageFormatException() {
        super("Error while formatting/interpreting package.");
    }

    public PackageFormatException(String msg) {
        super("Error while formatting/interpreting package." + msg);
    }
}
