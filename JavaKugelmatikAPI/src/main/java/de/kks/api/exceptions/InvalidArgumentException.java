package de.kks.api.exceptions;

public class InvalidArgumentException extends Exception {

    public InvalidArgumentException() {
        super("Invalid argument given while performing command.");
    }

    public InvalidArgumentException(String msg) {
        super("Invalid argument given while performing command: " + msg);
    }
}
