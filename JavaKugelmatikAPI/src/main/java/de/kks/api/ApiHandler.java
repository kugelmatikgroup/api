package de.kks.api;
import de.kks.api.command.Command;
import de.kks.api.command.CommandHandler;
import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ApiHandler {

    public static ApiHandler INSTANCE;
    private final IpClient ipClient;
    private final CommandHandler commandHandler;

    /**
     *To use functions of the Api, an object of the ApiHandler needs to be created.
     *A connection to the api server will be established automatically.
     * @author <a href="https://github.com/suchtpotenzial">Pascal Schneider</a>
     * @throws IOException
     */
    public ApiHandler() {
        commandHandler = new CommandHandler(this);
        ipClient = new IpClient(this);
        ipClient.connect();
    }

    public ApiHandler(String host, int port) {
        commandHandler = new CommandHandler(this);
        ipClient = new IpClient(this, host, port);
        ipClient.connect();
    }

    public Object sendCommand(String cmd_name) throws UnregisteredCommandException, InvalidArgumentException {
        Command cmd = getCommandHandler().getCommandByName(cmd_name);
        if (cmd == null) {
            throw new UnregisteredCommandException(cmd_name);
        }
        if (!cmd.containsAllRequiredArguments(new ArrayList<>())) {
            throw new InvalidArgumentException(cmd_name + ": Missing hard argument(s). Check your command arguments");
        }

        return ipClient.handleCommandConversation(new Package(cmd.getCommandName()));
    }

    public Object sendCommand(String cmd_name, Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        Command cmd = getCommandHandler().getCommandByName(cmd_name);
        if (cmd == null) {
            throw new UnregisteredCommandException(cmd_name);
        }
        if (!cmd.containsAllRequiredArguments(List.of("coordinates"))) {
            throw new InvalidArgumentException(cmd_name + ": Missing hard argument(s). Check your command arguments");
        }

        return ipClient.handleCommandConversation(new Package(cmd.getCommandName(), Arrays.stream(coordinates).toList()));
    }

    public Object sendCommand(String cmd_name, HashMap<String, Object> args, Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        Command cmd = getCommandHandler().getCommandByName(cmd_name);
        if (cmd == null) {
            throw new UnregisteredCommandException(cmd_name);
        }
        List<String> allArgs = new ArrayList<>(args.keySet());
        allArgs.add("coordinates");
        if (!cmd.containsAllRequiredArguments(allArgs)) {
            throw new InvalidArgumentException(cmd_name + ": Missing hard argument(s). Check your command arguments");
        }
        if (cmd.containsInvalidArgument(allArgs)) {
            throw new InvalidArgumentException(cmd_name + ": Invalid argument(s) given. Check your command arguments");
        }

        return ipClient.handleCommandConversation(new Package(cmd.getCommandName(), Arrays.stream(coordinates).toList(),args));
    }

    public Object sendCommand(String cmd_name, HashMap<String, Object> args) throws UnregisteredCommandException, InvalidArgumentException {
        Command cmd = getCommandHandler().getCommandByName(cmd_name);
        if (cmd == null) {
            throw new UnregisteredCommandException(cmd_name);
        }

        List<String> allArgs = new ArrayList<>(args.keySet());
        if (!cmd.containsAllRequiredArguments(allArgs)) {
            throw new InvalidArgumentException(cmd_name + ": Missing hard argument(s). Check your command arguments");
        }
        if (cmd.containsInvalidArgument(allArgs)) {
            throw new InvalidArgumentException(cmd_name + ": Invalid argument(s) given. Check your command arguments");
        }

        return ipClient.handleCommandConversation(new Package(cmd.getCommandName(),args));
    }

    /**
     * Set given Steppers to a desired height
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @param height Set an argument for the execution of the command
     *               Get all possible arguments through the commandHandler
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String setStepper(int height, Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("height",height);
        return String.valueOf(sendCommand("set",args, coordinates));
    }

    /**
     * Set all Steppers to a desired height
     * @param height Set an argument for the execution of the command
     *            Get all possible arguments through the commandHandler
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String setAll(int height) throws UnregisteredCommandException, InvalidArgumentException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("height", height);
        return String.valueOf(sendCommand("set", args, new Coordinate(-1, -1, -1, -1)));
    }

    /**
     * Set all Steppers of a cluster to a desired height
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @param height Set an argument for the execution of the command
     *            Get all possible arguments through the commandHandler
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String setCluster(int height, Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        for (Coordinate coord : coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);
        }
        HashMap<String, Object> args = new HashMap<>();
        args.put("height", height);
        return String.valueOf(sendCommand("set", args, coordinates));
    }

    /**
     * Lets you roll the given Stepper all the way in.
     * @WARNING Use with caution! Only use for debug purpose, because Spheres might fall off due to tension
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String sendHome(Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        return String.valueOf(sendCommand("home", coordinates));
    }

    /**
     * Lets you roll the Stepper of the given Cluster all the way in.
     * @WARNING Use with caution! Only use for debug purpose, because Spheres might fall off due to tension
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String sendClusterHome(Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        for (Coordinate coord : coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);
        }
        return String.valueOf(sendCommand("home", coordinates));
    }

    /**
     * Lets you roll all Steppers all the way in.
     * @WARNING Use with caution! Only use for debug purpose, because Spheres might fall off due to tension
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String sendAllHome() throws UnregisteredCommandException, InvalidArgumentException {
        return String.valueOf(sendCommand("home", new Coordinate(-1, -1, -1, -1)));
    }

    /**
     * Stops all current movements of given Stepper
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String stopStepper(Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        return String.valueOf(sendCommand("stop", coordinates));
    }

    /**
     * Stops all current movements of given Cluster
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String stopCluster(Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        for (Coordinate coord : coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);
        }
        return String.valueOf(sendCommand("stop", coordinates));
    }

    /**
     * Stops all current movements
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String stopAll() throws UnregisteredCommandException, InvalidArgumentException {
        return String.valueOf(sendCommand("stop", new Coordinate(-1, -1, -1, -1)));
    }

    /**
     * Lets you roll the given Stepper all the way out and in again. It's used to fix steppers which are rolled in wrongly
     * @WARNING Use with caution! Only use for debug purpose, because Spheres might fall off due to tension
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String fixKugel(Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        return String.valueOf(sendCommand("fix", coordinates));
    }

    /**
     * Lets you roll the Steppers of the given Cluster all the way out and in again. It's used to fix steppers which are rolled in wrongly
     * @WARNING Use with caution! Only use for debug purpose, because Spheres might fall off due to tension
     * @param coordinates coordinates of the stepper you want to address. Format: (ClusterX, ClusterY, StepperX, StepperY)
     *                   Using -1 will represent all possible coordinates
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String fixCluster(Coordinate... coordinates) throws UnregisteredCommandException, InvalidArgumentException {
        for (Coordinate coord : coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);
        }
        return String.valueOf(sendCommand("fix", coordinates));
    }

    /**
     * Lets you roll all Steppers all the way out and in again. It's used to fix steppers which are rolled in wrongly
     * @WARNING Use with caution! Only use for debug purpose, because Spheres might fall off due to tension
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String fixAll() throws UnregisteredCommandException, InvalidArgumentException {
        return String.valueOf(sendCommand("fix", new Coordinate(-1, -1, -1, -1)));
    }

    /**
     * Lets you play a desired registered choreography. The calculations will be done by the Kugelmatik-Core
     * @param choreographyName the name of the desired choreography (check available choreographies in initiation protocol)
     * @param targetFPS the desired frames per second the choreography will be played at (WARNING: start with a low frame rate)
     * @throws UnregisteredCommandException
     * @throws InvalidArgumentException
     */
    public String playChoreography(String choreographyName, float targetFPS) throws UnregisteredCommandException, InvalidArgumentException {
        HashMap<String, Object> args = new HashMap<>();
        args.put("choreo", choreographyName);
        args.put("targetFPS", targetFPS);
        return String.valueOf(sendCommand("choreo", args));
    }


    /**
     * This method is used to close the connection to the api server.
     * Only use this once at the end, when shutting down the application.
     */
    public void close() {
        //send package to tell the server that the socket will be closed
        try {
            sendCommand("close");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
        ipClient.close();
    }
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }
}
