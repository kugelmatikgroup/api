package de.kks.api;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;

public class Package {
    private final String cmd_name;

    //private final List<Coordinate> coordinates;
    private HashMap<String, Object> args = new HashMap<>();

    public Package(String cmd_name, List<Coordinate> coordinates) {
        this.cmd_name = cmd_name;
        args.put("coordinates", coordinates);
    }

    public Package(String cmd_name, List<Coordinate> coordinates, HashMap<String, Object> args) {
        this.cmd_name = cmd_name;
        args.put("coordinates", coordinates);
        this.args = args;
    }

    public Package(String cmd_name, HashMap<String, Object> args) {
        this.cmd_name = cmd_name;
        this.args = args;
    }

    public Package(String cmd_name) {
        this.cmd_name = cmd_name;
        this.args = new HashMap<>();
    }

    public String serialize() {
        /*
        String formattedCoordinateList = "";
        try {
            coordinates.get(0);
            formattedCoordinateList = Coordinate.serializeList(coordinates);
        } catch (NullPointerException | IndexOutOfBoundsException e){
            formattedCoordinateList = "[]";
        }

         */

        Gson gson = new Gson();
        String data = gson.toJson(this);

        //String data = "{cmd:" + cmd_name + "; coordinates:" + formattedCoordinateList + "; args:" + arg + "}";
        return data;
    }


    public String getCmdName() {
        return cmd_name;
    }

    public HashMap<String, Object> getArgs() {
        return args;
    }

    public Object getArgByKey(String key) {
        return args.get(key);
    }
}