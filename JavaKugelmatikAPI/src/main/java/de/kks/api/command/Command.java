package de.kks.api.command;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Command {

    private String cmd_name;
    private CommandType cmdType;
    private String description;
    private List<String> softArguments;
    private List<String> hardArguments;

    public Command(String cmd_name, CommandType cmdType, String description, List<String> hardArguments, List<String> softArguments) {
        this.cmd_name = cmd_name;
        this.cmdType = cmdType;
        this.description = description;
        this.hardArguments = hardArguments;
        this.softArguments = softArguments;
    }

    public Command(String cmd_name, CommandType cmdType, String description, List<String> hardArguments) {
        this.cmd_name = cmd_name;
        this.cmdType = cmdType;
        this.description = description;
        this.hardArguments = hardArguments;
        this.softArguments = new ArrayList<>();
    }

    public String getCommandName() {
        return cmd_name;
    }
    public String getDescription() {
        return description;
    }

    public List<String> getHardArguments() {
        return hardArguments;
    }

    public List<String> getSoftArguments() {
        return softArguments;
    }

    public CommandType getCommandType() {
        return cmdType;
    }

    public boolean containsAllRequiredArguments(List<String> args) {
        for(String hard : hardArguments) {
            if (!args.contains(hard)) return false;
        }
        return true;
    }

    public boolean containsInvalidArgument(List<String> args) {
        List<String> allArguments = Stream.concat(hardArguments.stream(), softArguments.stream()).toList();
        for (String arg : args) {
            if (!allArguments.contains(arg)) return true;
        }
        return false;
    }
}
