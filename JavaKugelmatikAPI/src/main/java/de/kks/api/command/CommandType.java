package de.kks.api.command;

public enum CommandType {
    ACTION,
    INFORMATION,
    EMPTY,
    REGISTER;
}
