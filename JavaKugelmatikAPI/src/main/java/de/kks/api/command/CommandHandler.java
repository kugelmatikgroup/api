package de.kks.api.command;

import de.kks.api.ApiHandler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CommandHandler {

    public HashMap<String,Command> registeredCommands;

    private ApiHandler apiHandler;

    public CommandHandler(ApiHandler apiHandler) {
        this.apiHandler = apiHandler;
        this.registeredCommands = new HashMap<>();
    }

    public Command getCommandByName(String cmd_name) {
        return registeredCommands.get(cmd_name);
    }

    public void registerCommand(String cmd_name, CommandType cmdType, String description, List<String> hardArguments, List<String> softArguments) {
        registeredCommands.put(cmd_name.toLowerCase(),new Command(cmd_name, cmdType, description, hardArguments, softArguments));
    }

    public void registerCommand(String cmd_name, CommandType cmdType, String description, List<String> hardArguments) {
        registeredCommands.put(cmd_name.toLowerCase(),new Command(cmd_name, cmdType, description, hardArguments));
    }

    public void registerCommand(Command command) {
        registeredCommands.put(command.getCommandName().toLowerCase(),command);
    }

    public HashMap<String, Command> getRegisteredCommands() {
        return registeredCommands;
    }

    public List<String> getArgumentList(String... args) {
        return Arrays.stream(args).toList();
    }
}
