package de.kks.api;


import de.kks.api.command.Command;

import java.util.HashMap;

public class InitProtocolData {

    public int kugelmatikWidth;
    public int kugelmatikHeight;
    public int clusterWidth;
    public int clusterHeight;
    public int maxHeight;
    public HashMap<String, Command> commands;
}
