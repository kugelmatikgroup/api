package de.kks.tempServer;

import java.util.ArrayList;
import java.util.List;

public class CoordinateHandler {

    private TempServer server;
    private List<ServerCoordinate> coordinates;

    public CoordinateHandler(TempServer server) {
        this.server = server;
        coordinates = new ArrayList<>();

        //Füge alle möglichen Coordinates hinzu
        for (int i = 1; i <= server.CLUSTER_HEIGHT; i++) {
            for (int j = 1; j <= server.CLUSTER_WIDTH; j++) {
                for (int k = 1; k <= server.KUGELMATIK_HEIGHT; k++) {
                    for (int l = 1; l <= server.KUGELMATIK_WIDTH; l++) {
                        coordinates.add(new ServerCoordinate(l,k,j,i));
                    }
                }
            }
        }
    }

    public List<ServerCoordinate> filterByRawCoordinate(int[] c) {
        //c length: 4
        List<ServerCoordinate> filtered = new ArrayList<>();
        for (ServerCoordinate all : coordinates) {
            if ((c[0] == -1 || all.getClusterX() == c[0]) && (c[1] == -1 || all.getClusterY() == c[1]) && (c[2] == -1 || all.getSphereX() == c[2]) && (c[3] == -1 || all.getSphereY() == c[3])) {
                filtered.add(all);
            }
        }
        return filtered;
    }



    public List<ServerCoordinate> getCoordinates() {
        return coordinates;
    }
}
