package de.kks.tempServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private final TempServer server;
    private final Socket socket;
    private final DataInputStream in;
    private final DataOutputStream out;

    public ClientHandler(TempServer server, Socket socket) {
        this.socket = socket;
        this.server = server;
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        /**
         * initiation protocol
         *
         * @format {KUGELMATIK_WIDTH;KUGELMATIK_HEIGHT;CLUSTER_WIDTH;CLUSTER_HEIGHT;KUGELMATIK_MAX_HEIGHT;[cmd_name:arg_type(arg_list):description/cmd_name:arg_type(arg_list):description...]}
         * @arg_type NONE, HEIGHT, CUSTOM(arg1,arg2...)
         */

        //This is just a dummy! Normally you'd extract the needed data from server variables
        send("{5;5;5;6;8000;[set:ACTION:HEIGHT():Set components to desired height/home:ACTION:NONE():Move stepper all the way to the top/fix:ACTION:NONE():roll stepper all the way down and up again/choreo:ACTION:CUSTOM(sine,ripple,plane):Lets you play a choreogaphy]}");
        

        while (true) {
            String received = receiveMessages(); //waits until a message is received
            ServerPackage pac = ServerPackage.deserialize(received);
            System.out.println("deserialized package: " + pac.serialize());

            if (pac.getCmd().equalsIgnoreCase("close")) {
                server.closeConnection(socket);
                return;
            }

            //If this is not given, an exeception will be thown. That's because the server wants to send a message to the client, while the socket is closed.
            if (received == null) {
                server.closeConnection(socket);
                return;
            }
        }
    }

    public void send(String message) {
        try {
            out.writeUTF(message);
            System.out.println(server.PREFIX + "Sent to " + socket.getInetAddress().getHostAddress() + ": " + message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String receiveMessages() {
        try {
            String received = in.readUTF();
            System.out.println(server.PREFIX + "Received from " + socket.getInetAddress().getHostAddress() + ": " + received);
            return received;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public DataInputStream getIn() {
        return in;
    }

    public DataOutputStream getOut() {
        return out;
    }
}
