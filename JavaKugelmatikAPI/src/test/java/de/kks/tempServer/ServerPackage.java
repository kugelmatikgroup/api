package de.kks.tempServer;

import com.google.gson.Gson;
import de.kks.api.Coordinate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServerPackage {
    private final String cmd_name;

    private List<Coordinate> coordinates;
    private HashMap<String, Object> args = new HashMap<>();

    public ServerPackage(String cmd_name, List<ServerCoordinate> coordinates) {
        this.cmd_name = cmd_name;
        args.put("coordinates", coordinates);
    }

    public ServerPackage(String cmd_name, List<ServerCoordinate> coordinates, HashMap<String, Object> args) {
        this.cmd_name = cmd_name;
        args.put("coordinates", coordinates);
        this.args = args;
    }

    public ServerPackage(String cmd_name, HashMap<String, Object> args) {
        this.cmd_name = cmd_name;
        this.args = args;
    }

    public String serialize() {
        /*
        String formattedCoordinateList = "";
        try {
            coordinates.get(0);
            formattedCoordinateList = Coordinate.serializeList(coordinates);
        } catch (NullPointerException | IndexOutOfBoundsException e){
            formattedCoordinateList = "[]";
        }

         */

        Gson gson = new Gson();
        String data = gson.toJson(this);

        //String data = "{cmd:" + cmd_name + "; coordinates:" + formattedCoordinateList + "; args:" + arg + "}";
        return data;
    }

    public static ServerPackage deserialize(String data) {
        Gson gson = new Gson();
        ServerPackage pac = gson.fromJson(data, ServerPackage.class);
        Object foo = pac.getArgs().get("coordinates");
        if (foo == null) return pac;
        List<ServerCoordinate> serializedCoordinates = (List<ServerCoordinate>) foo;
        List<ServerCoordinate> deserializedCoordinateList = new ArrayList<>();
        for (ServerCoordinate c : serializedCoordinates) {
            deserializedCoordinateList.addAll(ServerCoordinate.deserializeCoordinate(c));
        }
        pac.args.put("coordinates", deserializedCoordinateList);
        return pac;
    }

    public String getCmd() {
        return cmd_name;
    }

    public HashMap<String, Object> getArgs() {
        return args;
    }

    public Object getArgByKey(String key) {
        return args.get(key);
    }
}

/*public class ServerPackage {
    private final String cmd_name;

    private final List<ServerCoordinate> coordinates;

    private String arg;

    public ServerPackage(String cmd_name) {
        this.cmd_name = cmd_name;
        this.coordinates = new ArrayList<>();
        this.arg = "";
    }

    public ServerPackage(String cmd_name, List<ServerCoordinate> coordinates) {
        this.cmd_name = cmd_name;
        this.coordinates = coordinates;
        this.arg = null;
    }

    public ServerPackage(String cmd_name, List<ServerCoordinate> coordinates, String arg) {
        this.cmd_name = cmd_name;
        this.coordinates = coordinates;
        this.arg = arg;
    }

    public String serialize() {
        String formattedCoordinateList = "";
        try {
            coordinates.get(0);
            formattedCoordinateList = ServerCoordinate.serializeList(coordinates);
        } catch (NullPointerException | IndexOutOfBoundsException e){
            formattedCoordinateList = "[]";
        }
        String data = "{cmd:" + cmd_name + "; coordinates:" + formattedCoordinateList + "; args:" + arg + "}";
        return data;
    }

    public static ServerPackage deserialize(String serialized) {
        String foo = serialized.substring(1,serialized.length()-1);
        String[] data = foo.split("; ") ;
        String serialized_cmd = data[0].substring(4); //4
        String serialized_coordinates = data[1].substring(12); //12
        String serialized_args = data[2].substring(5); //5
        //System.out.println("serialized_cmd:" + serialized_cmd);
        //System.out.println("serialized_coordinates:" + serialized_coordinates);
        //System.out.println("serialized_args:" + serialized_args);

        serialized_coordinates = serialized_coordinates.substring(1,serialized_coordinates.length()-1);
        List<ServerCoordinate> deserializedCoordinateList = new ArrayList<>();
        String[] coordinates = serialized_coordinates.split(",");
        if (coordinates.length != 0 && coordinates[0] != "") {
            for (String c : coordinates) {
                deserializedCoordinateList.addAll(ServerCoordinate.deserializeCoordinate(c));
            }
        }
        return new ServerPackage(String.valueOf(serialized_cmd),deserializedCoordinateList,serialized_args);
    }



    public String getCmd() {
        return cmd_name;
    }

    public List<ServerCoordinate> getCoordinates() {
        return coordinates;
    }

    public String getArg() {
        return arg;
    }
}
 */
