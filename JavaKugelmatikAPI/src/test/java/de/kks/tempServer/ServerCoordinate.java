package de.kks.tempServer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ServerCoordinate {
    private int clusterX;

    private int clusterY;

    private int sphereX;

    private int sphereY;

    public ServerCoordinate() {
        this.clusterX = -1;
        this.clusterY = -1;
        this.sphereX = -1;
        this.sphereY = -1;
    }

    public ServerCoordinate(int clusterX, int clusterY) {
        this.clusterX = clusterX;
        this.clusterY = clusterY;
        this.sphereX = -1;
        this.sphereY = -1;
    }

    public ServerCoordinate(int clusterX, int clusterY, int sphereX, int sphereY) {
        this.clusterX = clusterX;
        this.clusterY = clusterY;
        this.sphereX = sphereX;
        this.sphereY = sphereY;
    }

    @Override
    public String toString() {
        return "[" +
                clusterX + "/" +
                clusterY + "/" +
                sphereX + "/" +
                sphereY +
                "]";
    }

    public static String serializeList(List<ServerCoordinate> coordinates) {
        //ignore ugly code ^^
        String coorFoo = "[";
        for (int i = 0; i < coordinates.size(); i++) {
            coorFoo += coordinates.get(i).toString() + (i==(coordinates.size()-1) ? "" : ",");
        }
        return coorFoo + "]";
    }

    public static Collection<? extends ServerCoordinate> deserializeCoordinate(ServerCoordinate undefinedCoordinate) {
        List<ServerCoordinate> deserializedCoordinates = new ArrayList<>();

        //example: [5/5/-1/-1]
        int[] coordinates = new int[]{undefinedCoordinate.clusterX, undefinedCoordinate.clusterY, undefinedCoordinate.sphereX, undefinedCoordinate.sphereY};
        deserializedCoordinates.addAll(TempServer.getServer().getCoordinateHandler().filterByRawCoordinate(coordinates));

        Collection<ServerCoordinate> collection = deserializedCoordinates;
        return collection;
    }

    public int getClusterX() {
        return clusterX;
    }

    public void setClusterX(int clusterX) {
        this.clusterX = clusterX;
    }

    public int getClusterY() {
        return clusterY;
    }

    public void setClusterY(int clusterY) {
        this.clusterY = clusterY;
    }

    public int getSphereX() {
        return sphereX;
    }

    public void setSphereX(int sphereX) {
        this.sphereX = sphereX;
    }

    public int getSphereY() {
        return sphereY;
    }

    public void setSphereY(int sphereY) {
        this.sphereY = sphereY;
    }
}
