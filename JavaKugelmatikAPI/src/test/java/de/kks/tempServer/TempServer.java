package de.kks.tempServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TempServer implements Runnable{

    public final String PREFIX = "Server -> ";

    private static TempServer server;
    private int port;
    private HashMap<ClientHandler,Thread> clientHandlers;
    private ServerSocket serverSocket;
    private CoordinateHandler coordinateHandler;

    public int CLUSTER_WIDTH;
    public int CLUSTER_HEIGHT;
    public int KUGELMATIK_WIDTH;
    public int KUGELMATIK_HEIGHT;
    public int KUGELMATIK_MAX_HEIGHT;

    public static void main(String[] args) {
        TempServer tempServer = new TempServer(6543);
        Thread serverThread = new Thread(tempServer);
        serverThread.start();
    }

    public TempServer(int port) {
        this.server = this;
        this.port = port;
        this.clientHandlers = new HashMap<>();

        //TODO durch Config Einträge ersetzten
        KUGELMATIK_WIDTH = 5;
        KUGELMATIK_HEIGHT = 5;
        CLUSTER_WIDTH = 5;
        CLUSTER_HEIGHT = 6;
        KUGELMATIK_MAX_HEIGHT = 8000;

        //Muss nach Laden der Config aufgerufen werden
        this.coordinateHandler = new CoordinateHandler(this);
    }

    public void processNewConnections() {
        System.out.println(PREFIX + "Server wartet auf Verbindungen");
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                ClientHandler clientHandler = new ClientHandler(this, socket);
                Thread handlerThread = new Thread(clientHandler);
                handlerThread.start();
                clientHandlers.put(clientHandler,handlerThread);
                System.out.println(PREFIX + "Ein Client hat die Verbindung hergestellt! ClientCount: " + clientHandlers.size());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void close() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void closeConnection(Socket socket) {
        try {
            socket.close();
            ClientHandler clientHandler = getClientHandlerBySocket(socket);
            clientHandlers.get(clientHandler).interrupt();
            clientHandlers.remove(getClientHandlerBySocket(socket));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void start() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        processNewConnections();
    }

    public ClientHandler getClientHandlerBySocket(Socket socket) {
        for (ClientHandler all : clientHandlers.keySet()) {
            if (all.getSocket().equals(socket)) return all;
        }
        return null;
    }

    @Override
    public void run() {
        start();
    }

    public static TempServer getServer() {
        return server;
    }

    public CoordinateHandler getCoordinateHandler() {
        return coordinateHandler;
    }
}
