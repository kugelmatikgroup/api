package de.kks.example;

import de.kks.api.ApiHandler;

public class Main {
    public static void main(String[] args) {
        ApiHandler apiHandler = new ApiHandler("127.0.0.1", 6543);

        //Add debug/sample code here

        apiHandler.close();
    }

    public static void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
