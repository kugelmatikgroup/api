package de.kks.example;

import de.kks.api.component.CustomComponent;

import java.net.Inet4Address;

public class ExampleComponent extends CustomComponent {
    public ExampleComponent(String componentName, Inet4Address ipAddress, String version, String description) {
        super(componentName, ipAddress, version, description);
    }

    @Override
    public String execute(String... args) {
        return null;
    }
}
